package com.example.socialnet;

import java.util.Date;

public class Persona {
    public String id;
    public String username;
    public String password;
    public Date crate_at;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getCrate_at() {
        return crate_at;
    }

    public void setCrate_at(Date crate_at) {
        this.crate_at = crate_at;
    }
}
