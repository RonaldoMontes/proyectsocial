package com.example.socialnet.Api.Servicios;


import com.example.socialnet.ViewModels.Mostrar_Usuarios;
import com.example.socialnet.ViewModels.Peticion_Detalles;
import com.example.socialnet.ViewModels.Peticion_Login;
import com.example.socialnet.ViewModels.Registro_Usuario;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ServicioPeticion {

    @FormUrlEncoded
    @POST("api/crearUsuario")
    abstract Call<Registro_Usuario> registrarUsuario(@Field("username") String correo, @Field("password") String contrasenia);

    @FormUrlEncoded
    @POST("api/login")
    Call<Peticion_Login> getLogin(@Field("username") String correo, @Field("password") String contrasenia);


    @POST("api/todosUsuarios")
    Call<Mostrar_Usuarios> getUsuario();

    @FormUrlEncoded
    @POST("api/detallesUsuario")
    Call<Peticion_Detalles> getDetlles(@Field("usuarioId") int id);
}
