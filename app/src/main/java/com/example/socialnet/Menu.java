package com.example.socialnet;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.socialnet.Api.Api;
import com.example.socialnet.Api.Servicios.ServicioPeticion;
import com.example.socialnet.ViewModels.Mostrar_Usuarios;
import com.example.socialnet.ViewModels.Peticion_Detalles;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Menu extends AppCompatActivity {
    private Button Mos, Det, cerrar;
    private EditText mostra, id, detail;
    private SharedPreferences pref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        Mos = (Button) findViewById(R.id.button3);
        mostra = (EditText) findViewById(R.id.editText3);
        id = (EditText) findViewById(R.id.editText4);
        detail = (EditText) findViewById(R.id.editText5);
        Det = (Button)findViewById(R.id.button4);
        cerrar=(Button)findViewById(R.id.button7);

        pref =  getSharedPreferences("credenciales", Context.MODE_PRIVATE);

        cerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CerrarSesion();
            }
        });

        Mos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ServicioPeticion s = Api.getApi(Menu.this).create(ServicioPeticion.class);
                Call<Mostrar_Usuarios> Registros = s.getUsuario();
                Registros.enqueue(new Callback<Mostrar_Usuarios>() {
                    @Override
                    public void onResponse(Call<Mostrar_Usuarios> call, Response<Mostrar_Usuarios> response) {
                        Mostrar_Usuarios mostrar = response.body();
                        String Cadena = "";
                        if (mostrar.estado.equals("true")){
                            for (Persona elemento: mostrar.usuarios
                                 ) {
                                Cadena = Cadena + elemento.id + " | " + elemento.username + " | " + elemento.password + " | " + elemento.crate_at +  "\n";
                                mostra.setText(Cadena);
                            }




                        }else{
                            Toast.makeText(Menu.this, "Datos Incorrectos", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Mostrar_Usuarios> call, Throwable t) {
                        Toast.makeText(Menu.this, "Error", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        Det.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ServicioPeticion s = Api.getApi(Menu.this).create(ServicioPeticion.class);
                int ids = Integer.parseInt(id.getText().toString()) ;
                Call<Peticion_Detalles> Detalles = s.getDetlles(ids);
                Detalles.enqueue(new Callback<Peticion_Detalles>() {
                    @Override
                    public void onResponse(Call<Peticion_Detalles> call, Response<Peticion_Detalles> response) {
                        Peticion_Detalles Mos_Det = response.body();

                        if (Mos_Det.estado.equals("true")){


                                detail.setText(Mos_Det.usuario + " | " + Mos_Det.password);




                        }else{
                            Toast.makeText(Menu.this, "Datos Incorrectos", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Peticion_Detalles> call, Throwable t) {
                        Toast.makeText(Menu.this, "Error", Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });
    }
    public void CerrarSesion(){
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }


}
