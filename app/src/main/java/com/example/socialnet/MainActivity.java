package com.example.socialnet;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.socialnet.Api.Api;
import com.example.socialnet.Api.Servicios.ServicioPeticion;
import com.example.socialnet.ViewModels.Peticion_Login;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private Button btn2,btn;
    private EditText ed1, ed2;
    private String APITOKEN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn2=(Button)findViewById(R.id.button2);
        btn=(Button)findViewById(R.id.button);
        ed1=(EditText) findViewById(R.id.editText);
        ed2=(EditText)findViewById(R.id.editText2);


        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new  Intent (getApplicationContext(),Registro.class);
                startActivity(intent);
            }
        });
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ServicioPeticion service = Api.getApi(MainActivity.this).create(ServicioPeticion.class);
                Call<Peticion_Login> loginCall =  service.getLogin(ed1.getText().toString(),ed2.getText().toString());

                loginCall.enqueue(new Callback<Peticion_Login>() {
                    @Override
                    public void onResponse(Call<Peticion_Login> call, Response<Peticion_Login> response) {
                        Peticion_Login peticion = response.body();
                        if(peticion.estado == "true"){
                            APITOKEN = peticion.token;
                            Verificar();
                            guardarPreferencias();

                            Toast.makeText(MainActivity.this, "Bienvenido", Toast.LENGTH_LONG).show();
                            startActivity(new Intent(MainActivity.this, Menu.class));
                        }else{
                            Toast.makeText(MainActivity.this, "Datos Incorrectos", Toast.LENGTH_LONG).show();
                        }
                    }
                    @Override
                    public void onFailure(Call<Peticion_Login> call, Throwable t) {
                        Toast.makeText(MainActivity.this, "Erro ", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }
    public void guardarPreferencias () {
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token = APITOKEN;
        SharedPreferences.Editor editor = preferencias.edit();
        editor.putString("TOKEN", token);
        editor.commit();
    }
    public void Verificar () {
        // -- Verificar si tiene una sesión iniciada
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token = preferencias.getString("TOKEN", "");
        if (token != "") {
            Toast.makeText(MainActivity.this, "Bienvenido Nuevamente", Toast.LENGTH_LONG).show();
            startActivity(new Intent(MainActivity.this, Menu.class));
        }

    }

}
