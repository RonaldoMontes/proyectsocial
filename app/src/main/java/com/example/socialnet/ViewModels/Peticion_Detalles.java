package com.example.socialnet.ViewModels;

import com.example.socialnet.Person;
import com.example.socialnet.Persona;

import java.util.List;

public class Peticion_Detalles {
    public String estado;
    public String usuario;
    public String password;

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
