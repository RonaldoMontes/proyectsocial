package com.example.socialnet.ViewModels;

import com.example.socialnet.Persona;

import java.lang.reflect.Array;
import java.util.List;

public class Mostrar_Usuarios {
    public String estado;
    public List<Persona> usuarios;
    public String detalle;

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public List<Persona> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<Persona> usuarios) {
        this.usuarios = usuarios;
    }
}
